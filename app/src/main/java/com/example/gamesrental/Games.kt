package com.example.gamesrental

import java.io.Serializable

class Games() :Serializable {
    var id:Int = 0
    var name: String? = null
    var image:String? = null
    var release_date:String? = null
    var trailer:String? = null
    var platforms:ArrayList<String> = ArrayList()
}