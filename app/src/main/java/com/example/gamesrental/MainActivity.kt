package com.example.gamesrental

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.example.gamesrental.retrofit.GamesApi
import com.example.gamesrental.retrofit.GamesDataClass
import com.example.gamesrental.retrofit.RecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.* //Faz o findViewById ser inútil!
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GamesApi()
            .getGames().enqueue(object: Callback<GamesDataClass>{
            override fun onResponse(call: Call<GamesDataClass>, response: Response<GamesDataClass>) {
                val gamesResponse = response.body()
                /*
                * Há duas maneiras de usar os dados, 1 é utilizando "na mão", como pode ser visto no Log HelpMe2
                * A outra é usando let, que trata os nulos automaticamente. Quando se usa o let, ele trata o nulo
                * e joga o valor para a variável it.
                * Log.d("HelpMe2", gamesResponse?.games?.get(0)?.name)
                * */
                gamesResponse?.let {
                    rc.layoutManager = LinearLayoutManager(applicationContext) //Instancia o layout do recyclerView

                    //Adiciona o adaptador ao recyclerView
                    val adapter = RecyclerViewAdapter(it.games) { game ->
                        var intent = Intent(applicationContext, ShowGamesActivity::class.java)
                        intent.putExtra("game", game)
                        applicationContext.startActivity(intent)
                    }
                    rc.adapter = adapter
                }
            }

            override fun onFailure(call: Call<GamesDataClass>, t: Throwable) {
                Log.d("HelpMe", t.message)
            }

        })
    }
}



