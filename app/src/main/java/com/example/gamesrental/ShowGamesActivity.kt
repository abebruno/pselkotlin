package com.example.gamesrental

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.gamesrental.retrofit.GlideApp
import kotlinx.android.synthetic.main.activity_show_games.*

/*
Glide é uma biblioca que recupera as imagens a partir de requisiões
HTTP de maneira assíncrona.
* with() -> pega o contexto
* load() -> recupera a imagem pela URL
* error() -> coloca uma imagem caso algum erro aconteça
* into() -> coloca a imagem no imageView
* */
class ShowGamesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_games)

        val game = this.intent?.extras?.getSerializable("game") as Games

        showNametextView.setText(game.name)
        showReleasetextView.setText(game.release_date)
        showPlataformTextView.setText(this.makePlat(game.platforms))
        showTrailerTextView.setText(game.trailer)
        showTrailerTextView.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse(game.trailer))
            startActivity(intent)

            GlideApp.with(this).load(game.image).error(R.drawable.no_image).into(showGameImageView);
        }
    }

    fun makePlat(plat:ArrayList<String>):String{
        var returnPlat = " "

        for(i in 0 until plat.size){
            returnPlat += plat[i] + " "
        }

        return returnPlat
    }
}
