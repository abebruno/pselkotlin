package com.example.gamesrental.retrofit

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

val BASE_URL = "https://dl.dropboxusercontent.com/s/1b7jlwii7jfvuh0/"

/*
* O JSON que a gente recebe é bem traiçoeiro!
* É um JSON com apenas um campo chamado "games". Dentro desse campo, há uma lista
* de games.
* */
interface GamesApi {
    @GET("game")
    fun getGames(): Call<GamesDataClass> //Equivale ao único campo do JSON, "games"

    companion object{
        operator fun invoke(): GamesApi {
            return Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build().create(
                GamesApi::class.java)
        }
    }
}