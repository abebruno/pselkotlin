package com.example.gamesrental.retrofit

import com.example.gamesrental.Games
import com.google.gson.annotations.SerializedName

data class GamesDataClass(
    @SerializedName("games")
    val games:ArrayList<Games> //Uma lista de games é retornada no campo "games"
)