package com.example.gamesrental.retrofit

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gamesrental.Games
import com.example.gamesrental.R
import kotlinx.android.synthetic.main.rc_layout.view.*
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
/*
Estou usando uma função lambda como clickListener. Passo a referencia no construtor do adaptador como listener e ligo
o listener ao itemViewHolder no viewHolder. Defino qual será a função no MainActivity!
* */

@GlideModule
open class AppGlideModule : AppGlideModule()

//Adiciona o listenner dentro do viewHolder
class RecyclerViewAdapter(private var itens: ArrayList<Games>, var listener: (Games) -> Unit) : Adapter<RecyclerViewAdapter.GamesViewHolder>( ){

    //Cria o ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): GamesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rc_layout, parent, false)
        return GamesViewHolder(view)
    }

    //Numero de itens
    override fun getItemCount(): Int {
        return this.itens.size
    }

    //Faz o bind
    override fun onBindViewHolder(p0: GamesViewHolder, p1: Int) {
        p0.bind(this.itens.get(p1))
    }

    //Classe do ViewHolder
    //Na View, adicionar um clickListener!

    inner class GamesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private var posi: Int = 0
        private var viewName = itemView.textViewGameName
        private var viewDate = itemView.textViewReleaseDate
        private var imageView = itemView.imageViewGame

        //Função de click é linkada aqui
        fun bind(game: Games) {
            this.viewName.setText(game.name)
            this.viewDate.setText(game.release_date)
            itemView.setOnClickListener { listener(game) }

            GlideApp.with(itemView.context).load(game.image).error(R.drawable.no_image).into(imageView)
        }
    }
}

